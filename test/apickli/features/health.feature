@intg
Feature: API proxy health
	As API administrator
    I want to monitor Apigee proxy and backend service health
    So I can alert when it is down
    
	@get-ping
    Scenario: Verify the API proxy is responding
        Given I have valid credentials
		When I GET /ping
        Then response code should be 200
        And response header Content-Type should be application/json
        And response body path $.apiproxy should be paymentsjs-v2
        And response body path $.client should be ^\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3}$
        And response body path $.latency should be ^\d{1,2}
        And response body path $.message should be PONG
        