@intg
@payment
@card
Feature: Card Payments
    As a PaymentsJS developer
    I want a functional endpoint
    So that the library consumers can run credit card payments
    
    @cardPaymentsSale
    Scenario Outline: basic sale transaction
        Given I have valid credentials
        And I have a valid request
        And I generate an authKey
        And I set card to { "number": "<CardNumber>", "expiration": "1220", "cvv": "<CVV>" }
        When I POST to /api/payment/card
        Then response code should be 201
        And response body should be valid json
        And response body should contain APPROVED
		And response body path $.gatewayResponse.status should be Approved
		And response body path $.gatewayResponse.message should be APPROVED 000001
		And response body path $.gatewayResponse.code should be 000001
		And response body path $.gatewayResponse.reference should be `reference`
		Examples:
		|	CardName	|	CardNumber		 | CVV  |
		|	Visa		|	4111111111111111 | 222  |
		|	Mastercard	|	5454545454545454 | 333  |
		|	Discover	|	6011000993026909 | 444  |
		|	Amex		|	371449635392376  | 5555 |
		
		
	# ++++++++++++++ Negative Scenarios +++++++++++++++++++++++
	
	@cardPayment_byMisMatchCVVRange
    Scenario Outline: basic sale transaction with Mismatch CVV range
        Given I have valid credentials
        And I have a valid request
        And I generate an authKey
        And I set card to { "number": "<CardNumber>", "expiration": "1220", "cvv":"<CVV>" }
        When I POST to /api/payment/card
        Then response code should be 400
        And response body should be valid json
        And response body path $.gatewayResponse.code should be 400000
		And response body path $.gatewayResponse.message should be There was a problem with the request. Please see 'detail' for more. 
		And response body path $.gatewayResponse.detail should be Invalid code
	Examples:
		|	CardName	|	CardNumber		 | CVV   |
		|	Visa		|	4111111111111111 | 2222  |
		|	Mastercard	|	5454545454545454 | 3333  |
		|	Discover	|	6011000993026909 | 4444  |
		|	Amex		|	371449635392376  | 555   |	

	@cardPaymentSale_byWithOutCardData
    Scenario: Try to perform a sale transaction withOut Card Data  
        Given I have valid credentials
        And I have a valid request
        And I generate an authKey
		And I set card to {}     
        When I POST to /api/payment/card
        Then response code should be 400
        And response body should be valid json
		And response body path $.gatewayResponse.code should be 400000
		And response body path $.gatewayResponse.message should be There was a problem with the request. Please see 'detail' for more. 
		And response body path $.gatewayResponse.detail should be request.ECommerce.CardData: The Number field is required.; request.ECommerce.CardData: The Expiration field is required.	
			
	@cardPaymentSale_byBlankCardNumber
    Scenario: Try to perform a sale transaction withOut Card Number  
        Given I have valid credentials
        And I have a valid request
        And I generate an authKey
		And I set card to {"expiration": "2018", "cvv":"123" }     
        When I POST to /api/payment/card
        Then response code should be 400
        And response body should be valid json
		And response body path $.gatewayResponse.code should be 400000
		And response body path $.gatewayResponse.message should be There was a problem with the request. Please see 'detail' for more. 
		And response body path $.gatewayResponse.detail should be The Number field is required.	

	@cardPaymentSale_byBlankExpireDate
    Scenario: Try to perform a sale transaction withOut Expire Data 
        Given I have valid credentials
        And I have a valid request
        And I generate an authKey
		And I set card to { "number": "4111111111111111", "cvv":"123" }     
        When I POST to /api/payment/card
        Then response code should be 400
        And response body should be valid json
		And response body path $.gatewayResponse.code should be 400000
		And response body path $.gatewayResponse.message should be There was a problem with the request. Please see 'detail' for more. 
		And response body path $.gatewayResponse.detail should be The Expiration field is required.	

	@cardPaymentSale_byBlankCVV
    Scenario: Try to perform a sale transaction withOut CVV Data 
        Given I have valid credentials
        And I have a valid request
        And I generate an authKey
		And I set card to { "number": "4111111111111111", "expiration": "2018"}     
        When I POST to /api/payment/card
        Then response code should be 400
        And response body should be valid json
		And response body path $.gatewayResponse.code should be 400000
		And response body path $.gatewayResponse.message should be There was a problem with the request. Please see 'detail' for more. 
		And response body path $.gatewayResponse.detail should be The CVV field is required.		
		
	@cardPaymentSale_byInvalidCardNumber
    Scenario: Try to perform a sale transaction with invalid Card Number 
        Given I have valid credentials
        And I have a valid request
        And I generate an authKey
		And I set card to { "number":"9111111911111119", "expiration":"1220", "cvv":"123" }   
        When I POST to /api/payment/card
        Then response code should be 404
        And response body should be valid json
		And response body path $.gatewayResponse.code should be 400000
		And response body path $.gatewayResponse.message should be There was a problem with the request. Please see 'detail' for more. 
		And response body path $.gatewayResponse.detail should be InvalidRequestData : INVALID C_CARDNUMBER		
		
	@cardPayment_byInvalidExpiryDate_One
    Scenario: Try to perform a sale transaction with invalid Expiry date
        Given I have valid credentials
        And I have a valid request
        And I generate an authKey
        And I set card to { "number": "4111111111111111", "expiration": "Test", "cvv":"123" }
        When I POST to /api/payment/card
        Then response code should be 400
        And response body should be valid json
        And response body path $.gatewayResponse.code should be 400000
		And response body path $.gatewayResponse.message should be There was a problem with the request. Please see 'detail' for more. 
		And response body path $.gatewayResponse.detail should be Expiration must be a number		
		
	@cardPayment_byInvalidExpiryDate_Two
    Scenario: Try to perform a sale transaction with Expired Card date
        Given I have valid credentials
        And I have a valid request
        And I generate an authKey
        And I set card to { "number": "4111111111111111", "expiration": "1211", "cvv":"123" }
        When I POST to /api/payment/card
        Then response code should be 400
        And response body should be valid json
        And response body path $.gatewayResponse.code should be 400000
		And response body path $.gatewayResponse.message should be There was a problem with the request. Please see 'detail' for more. 
	#	And response body path $.gatewayResponse.detail should be Credit card expired		
	
	@cardPayment_byExceedLimitExpiryDate
    Scenario: Try to perform a sale transaction with Exceed Limit ExpiryDate
        Given I have valid credentials
        And I have a valid request
        And I generate an authKey
        And I set card to { "number": "4111111111111111", "expiration": "12", "cvv":"123" }
        When I POST to /api/payment/card
        Then response code should be 400
        And response body should be valid json
        And response body path $.gatewayResponse.code should be 400000
		And response body path $.gatewayResponse.message should be There was a problem with the request. Please see 'detail' for more. 
		And response body path $.gatewayResponse.detail should be The field Expiration must be a string with a minimum length of 4 and a maximum length of 4

	@cardPaymentSale_byInvalidCVV
    Scenario: Try to perform a sale transaction with invalid CVV 
        Given I have valid credentials
        And I have a valid request
        And I generate an authKey
		And I set card to { "number": "4111111111111111", "expiration": "2018", "cvv":"Test" }  
        When I POST to /api/payment/card
        Then response code should be 400
        And response body should be valid json
		 And response body path $.gatewayResponse.code should be 400000
        And response body should contain There was a problem with the request. Please see 'detail' for more.
		And response body path $.gatewayResponse.detail should be INVALID C_CVV
		
	@cardPaymentSale_byExceedLimitCVV
    Scenario: Try to perform a sale transaction with Exceed Limit CVV 
        Given I have valid credentials
        And I have a valid request
        And I generate an authKey
		And I set card to { "number": "4111111111111111", "expiration": "2018", "cvv":"123456" }  
        When I POST to /api/payment/card
        Then response code should be 400
        And response body should be valid json
		 And response body path $.gatewayResponse.code should be 400000
        And response body should contain There was a problem with the request. Please see 'detail' for more.
		And response body path $.gatewayResponse.detail should be The field CVV must be a string or array type with a maximum length of '4'.
		