@intg
@vault
@ach
Feature: ACH Vault
    As a PaymentsJS developer
    I want a functional endpoint
    So that the library consumers can store bank information
    
    @achVaultBasic
    Scenario: basic vault request
        Given I have valid credentials
        And I have a valid request
        And I generate an authKey
        And I set ach to { "accountNumber": "12345678901234", "routingNumber": "056008849", "type": "Checking" }
        When I POST to /api/vault/ach
        Then response code should be 200
        And response body should be valid json
        And response body path $.gatewayResponse.vaultResponse.status should be 1
		And response body path $.gatewayResponse.vaultResponse.message should be SUCCESS 
		And response body path $.gatewayResponse.vaultResponse.data should be `token`
        And I store the vault token
		
	@achVaultBasic_ByAccountType
    Scenario Outline: ACH vault request by Account Types
        Given I have valid credentials
        And I have a valid request
        And I generate an authKey
        And I set ach to { "accountNumber": "12345678901234", "routingNumber": "056008849", "type": <accountType> }
        When I POST to /api/vault/ach
        Then response code should be 200
        And response body should be valid json
        And response body path $.gatewayResponse.vaultResponse.status should be 1
		And response body path $.gatewayResponse.vaultResponse.message should be SUCCESS 
		And response body path $.gatewayResponse.vaultResponse.data should be `token`
        And I store the vault token
	Examples:
	|accountType|
	|"Savings"  |
	|"Checking" |

    @achVaultCreate
    Scenario Outline: basic vault request with "create" explicated
        Given I have valid credentials
        And I have a valid request
        And I set ach to { "accountNumber": "12345678901234", "routingNumber": "056008849", "type": <accountType> }
        And I set vault to { "operation": "CREATE" }
        And I generate an authKey
        When I POST to /api/vault/ach
        Then response code should be 200
        And response body should be valid json
        And response body path $.gatewayResponse.vaultResponse.status should be 1
		And response body path $.gatewayResponse.vaultResponse.message should be SUCCESS 
		And response body path $.gatewayResponse.vaultResponse.data should be `token`
        And I store the vault token
	Examples:
	|accountType|
	|"Savings"  |
	|"Checking" |		

    @achVaultUpdate
    Scenario Outline: create a vault token then update it
        Given I have valid credentials
        And I have a valid request
        And I set ach to { "accountNumber": "12345678901234", "routingNumber": "056008849", "type": <CreateAccountType> }
        And I set vault to { "operation": "CREATE" }
        And I generate an authKey
        When I POST to /api/vault/ach
        Then response code should be 200
        And response body should be valid json
        And response body path $.gatewayResponse.vaultResponse.status should be 1
		And response body path $.gatewayResponse.vaultResponse.message should be SUCCESS 
		And response body path $.gatewayResponse.vaultResponse.data should be `token`
        And I store the vault token
        Given I have valid credentials
        And I have a valid request
        And I set ach to { "accountNumber": "12345678901234", "routingNumber": "056008849", "type": <UpdateAccountType> }
        And I set vault to { "operation": "UPDATE" }
        And I grab a vault token
        And I generate an authKey
        When I POST to /api/vault/ach
        Then response code should be 200
        And response body should be valid json
        And response body path $.vaultResponse.status should be 1
		And response body path $.vaultResponse.message should be SUCCESS 
		And response body path $.vaultResponse.data should be `token`
        And the vault token should match
	Examples:
	|CreateAccountType|UpdateAccountType|
	|"Savings"  |"Checking" |
	|"Checking" |"Savings"  |
	|"Checking" |"Checking" |
	|"Savings"  |"Savings"  |
	
	@achVaultDelete
    Scenario Outline: create a vault token then delete it
        Given I have valid credentials
        And I have a valid request
        And I set ach to { "accountNumber": "12345678901234", "routingNumber": "056008849", "type": <accountType> }
        And I set vault to { "operation": "CREATE" }
        And I generate an authKey
        When I POST to /api/vault/ach
        Then response code should be 200
        And response body should be valid json
        And response body should contain SUCCESS
        And response body should contain data
        And I store the vault token
        Given I have valid credentials
        And I have a valid request
        And I set vault to { "operation": "DELETE" }
        And I grab a vault token
        And I generate an authKey
        When I POST to /api/vault/ach
        Then response code should be 200
        And response body should be valid json
		And response body path $.vaultResponse.status should be 1
		And response body path $.vaultResponse.message should be DELETED 
		And response body path $.vaultResponse.data should be `token`
        And the vault token should match
	Examples:
	|accountType|
	|"Savings"  |
	|"Checking" |
		
	# +++++++++++++++++++++ Negative Scenarios +++++++++++++++++++++		
	
	@achVault_byInvalidAccountType
    Scenario: basic Ach Vault transaction by Invalid Account Type
        Given I have valid credentials
        And I have a valid request
        And I generate an authKey
        And I set ach to { "accountNumber": "12345678901234", "routingNumber": "056008849", "type": "Test" }
        And I have valid billing data
        When I POST to /api/vault/ach
        Then response code should be 404
        And response body should be valid json
        And response body path $.gatewayResponse.code should be 400000
		And response body path $.gatewayResponse.message should be There was a problem with the request. Please see 'detail' for more. 
		And response body path $.gatewayResponse.detail should be The Type field is required.; Error converting value "Test" to type		
	
	@achVault_byWithOutAccountNumberTag
    Scenario: basic Ach Vault transaction by WithOut Account Number tag
        Given I have valid credentials
        And I have a valid request
        And I generate an authKey
        And I set ach to {"routingNumber": "056008849", "type": "Checking" }
        When I POST to /api/vault/ach
        Then response code should be 400
        And response body should be valid json
        And response body path $.gatewayResponse.code should be 100008
		And response body path $.gatewayResponse.message should be Invalid message request content
		And response body path $.gatewayResponse.detail should be Required content: account\(routingNumber, accountNumber, type\)	
		
	@achVault_byWithOutRoutingNumberTag
    Scenario: basic Ach Vault transaction by WithOut Routing Number tag
        Given I have valid credentials
        And I have a valid request
        And I generate an authKey
        And I set ach to {  "accountNumber": "12345678901234",  "type": "Checking" }
        When I POST to /api/vault/ach
        Then response code should be 400
        And response body should be valid json
        And response body path $.gatewayResponse.code should be 100008
		And response body path $.gatewayResponse.message should be Invalid message request content
		And response body path $.gatewayResponse.detail should be Required content: account\(routingNumber, accountNumber, type\)
		
	@achVault_byWithOutTypeTag
    Scenario: basic Ach Vault transaction by WithOut Type tag
        Given I have valid credentials
        And I have a valid request
        And I generate an authKey
        And I set ach to {  "accountNumber": "12345678901234",  "routingNumber": "056008849"}
        When I POST to /api/vault/ach
        Then response code should be 400
        And response body should be valid json
		And response body path $.gatewayResponse.code should be 100008
		And response body path $.gatewayResponse.message should be Invalid message request content
		And response body path $.gatewayResponse.detail should be Required content: account\(routingNumber, accountNumber, type\)
		
	@achVault_byWithOutACHData
    Scenario: basic Ach Vault transaction by WithOut ACH Data
        Given I have valid credentials
        And I have a valid request
        And I generate an authKey
        And I set ach to {}
        When I POST to /api/vault/ach
        Then response code should be 400
        And response body should be valid json
        And response body path $.gatewayResponse.code should be 100008
		And response body path $.gatewayResponse.message should be Invalid message request content
		And response body path $.gatewayResponse.detail should be Required content: account\(routingNumber, accountNumber, type\)
		
	@achVaultCreate_byInvalidOperation
    Scenario: basic Ach vault request with invalid Operation for Create
        Given I have valid credentials
        And I have a valid request
        And I set ach to { "accountNumber": "12345678901234", "routingNumber": "056008849", "type": "Checking" }
        And I set vault to { "operation": "Test" }
        And I generate an authKey
        When I POST to /api/vault/ach
        Then response code should be 404
        And response body should be valid json
        And response body path $.gatewayResponse.code should be 100006
		And response body path $.gatewayResponse.message should be Resource not found
		And response body path $.gatewayResponse.detail should be There is no resource at the path specified		

	@achVaultUpdate_byInvalidOperation
    Scenario: create a Ach vault request with invalid Operation for Update
        Given I have valid credentials
        And I have a valid request
        And I set ach to { "accountNumber": "12345678901234", "routingNumber": "056008849", "type": "Checking" }
        And I set vault to { "operation": "CREATE" }
        And I generate an authKey
        When I POST to /api/vault/ach
        Then response code should be 200
        And response body should be valid json
        And response body path $.gatewayResponse.vaultResponse.status should be 1
		And response body path $.gatewayResponse.vaultResponse.message should be SUCCESS 
		And response body path $.gatewayResponse.vaultResponse.data should be `token`
        And I store the vault token
        Given I have valid credentials
        And I have a valid request
        And I set ach to { "accountNumber": "12345678901234", "routingNumber": "056008849", "type": "Checking" }
        And I set vault to { "operation": "Test" }
        And I grab a vault token
        And I generate an authKey
        When I POST to /api/vault/ach
        Then response code should be 404
        And response body should be valid json
		And response body path $.gatewayResponse.code should be 100006
		And response body path $.gatewayResponse.message should be Resource not found 
		And response body path $.gatewayResponse.detail should be There is no resource at the path specified		
	
	@achVaultDelete_byInvalidOperation
    Scenario: create a Ach vault request with invalid Operation for Delete
        Given I have valid credentials
        And I have a valid request
        And I set ach to { "accountNumber": "12345678901234", "routingNumber": "056008849", "type": "Checking" }
        And I set vault to { "operation": "CREATE" }
        And I generate an authKey
        When I POST to /api/vault/ach
        Then response code should be 200
        And response body should be valid json
        And response body should contain SUCCESS
        And response body should contain data
        And I store the vault token
        Given I have valid credentials
        And I have a valid request
        And I set vault to { "operation": "Test" }
        And I grab a vault token
        And I generate an authKey
        When I POST to /api/vault/ach
        Then response code should be 404
        And response body should be valid json
		And response body path $.gatewayResponse.code should be 100006
		And response body path $.gatewayResponse.message should be Resource not found 
		And response body path $.gatewayResponse.detail should be There is no resource at the path specified	
    
    @achVaultUpdate404
    Scenario: attempt to update nonexistent token
        Given I have valid credentials
        And I have a valid request
        And I set ach to { "accountNumber": "12345678901234", "routingNumber": "056008849", "type": "Savings" }
        And I set vault to { "operation": "UPDATE", "token": "NOTAREALTOKENLUL" }
        And I generate an authKey
        When I POST to /api/vault/ach
        Then response code should be 404
        And response body should be valid json
        And response body should contain info
		And response body path $.code should be 100006
		And response body path $.message should be Resource not found 
		And response body path $.detail should be UNABLE TO LOCATE

    @achVaultDelete404
    Scenario: attempt to delete nonexistent token
        Given I have valid credentials
        And I have a valid request
        And I set vault to { "operation": "DELETE", "token": "NOTAREALTOKENLUL" }
        And I generate an authKey
        When I POST to /api/vault/ach
        Then response code should be 404
        And response body should be valid json
        And response body should contain info
		And response body path $.code should be 100006
		And response body path $.message should be Resource not found 
		And response body path $.detail should be UNABLE TO LOCATE
		
		
		