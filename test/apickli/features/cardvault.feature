@intg
@vault
@card
Feature: Card Vault
    As a PaymentsJS developer
    I want a functional endpoint
    So that the library consumers can store credit cards
    
    @cardVaultBasic
    Scenario: basic vault request
        Given I have valid credentials
        And I have a valid request
        And I generate an authKey
        And I set card to { "number": "5454545454545454", "expiration": "1220" }
        When I POST to /api/vault/card
        Then response code should be 200
        And response body should be valid json
		And response body path $.gatewayResponse.vaultResponse.status should be 1
		And response body path $.gatewayResponse.vaultResponse.message should be SUCCESS 
		And response body path $.gatewayResponse.vaultResponse.data should be `token`	
        And I store the vault token
		
	@cardVaultBasic_WithAllCardTypes
    Scenario Outline: basic vault request
        Given I have valid credentials
        And I have a valid request
        And I generate an authKey
		And I set card to { "number": "<CardNumber>", "expiration": "<Expiration>" }
        When I POST to /api/vault/card
        Then response code should be 200
        And response body should be valid json
		And response body path $.gatewayResponse.vaultResponse.status should be 1
		And response body path $.gatewayResponse.vaultResponse.message should be SUCCESS 
		And response body path $.gatewayResponse.vaultResponse.data should be `token`		
        And I store the vault token
	Examples:
		|	CardName	|	CardNumber		 | Expiration  |
		|	Visa		|	4111111111111111 | 1220  |
		|	Mastercard	|	5454545454545454 | 1230  |
		|	Discover	|	6011000993026909 | 1240  |
		|	Amex		|	371449635392376  | 1250  |
		

    @cardVaultCreate_WithAllCardTypes
    Scenario Outline: basic vault request with "create" explicated
        Given I have valid credentials
        And I have a valid request
	    And I set card to { "number": <CardNumber>, "expiration": <Expiration> }
        And I set vault to { "operation": "CREATE" }
        And I generate an authKey
        When I POST to /api/vault/card
        Then response code should be 200
        And response body should be valid json
        And response body path $.gatewayResponse.vaultResponse.status should be 1
		And response body path $.gatewayResponse.vaultResponse.message should be SUCCESS 
		And response body path $.gatewayResponse.vaultResponse.data should be `token`		
        And I store the vault token
	Examples:
		|	CardName	|	CardNumber		 | Expiration  |
		|	Visa		|	4111111111111111 | 1220  |
		|	Mastercard	|	5454545454545454 | 1230  |
		|	Discover	|	6011000993026909 | 1240  |
		|	Amex		|	371449635392376  | 1250  |

    @cardVaultUpdate_WithAllCardTypes
    Scenario Outline: create a vault token then update it
        Given I have valid credentials
        And I have a valid request
		And I set card to { "number": <Create_CardNumber>, "expiration": <Create_Expiration> }
        And I set vault to { "operation": "CREATE" }
        And I generate an authKey
        When I POST to /api/vault/card
        Then response code should be 200
        And response body should be valid json
        And response body path $.gatewayResponse.vaultResponse.status should be 1
		And response body path $.gatewayResponse.vaultResponse.message should be SUCCESS 
		And response body path $.gatewayResponse.vaultResponse.data should be `token`		
        And I store the vault token
        Given I have valid credentials
        And I have a valid request
		And I set card to { "number": <Update_CardNumber>, "expiration": <Update_Expiration> }
        And I set vault to { "operation": "UPDATE" }
        And I grab a vault token
        And I generate an authKey
        When I POST to /api/vault/card
        Then response code should be 200
        And response body should be valid json
		And response body path $.vaultResponse.status should be 1
		And response body path $.vaultResponse.message should be SUCCESS 
		And response body path $.vaultResponse.data should be `token`
        And the vault token should match
	Examples:
		| Create_CardNumber  | Update_CardNumber  | Create_Expiration  | Update_Expiration  |
		|	4111111111111111 |	371449635392376	  | 1220  			   | 1260 |
		|	5454545454545454 |	6011000993026909  | 1230  			   | 1270 |
		|	6011000993026909 |	5454545454545454  | 1240 			   | 1280 |
		|	371449635392376  |	4111111111111111  | 1250  			   | 1290 |
		
		
	@cardVaultDelete_WithAllCardTypes
    Scenario Outline: create a vault token then delete it
        Given I have valid credentials
        And I have a valid request
        And I set card to { "number": <CardNumber>, "expiration": <Expiration> }
        And I set vault to { "operation": "CREATE" }
        And I generate an authKey
        When I POST to /api/vault/card
        Then response code should be 200
        And response body should be valid json
        And response body path $.gatewayResponse.vaultResponse.status should be 1
		And response body path $.gatewayResponse.vaultResponse.message should be SUCCESS 
		And response body path $.gatewayResponse.vaultResponse.data should be `token`
        And I store the vault token
        Given I have valid credentials
        And I have a valid request
        And I set vault to { "operation": "DELETE" }
        And I grab a vault token
        And I generate an authKey
        When I POST to /api/vault/card
        Then response code should be 200
        And response body should be valid json
		And response body path $.vaultResponse.status should be 1
		And response body path $.vaultResponse.message should be DELETED 
		And response body path $.vaultResponse.data should be `token`
        And the vault token should match
	Examples:
		|	CardName	|	CardNumber		 | Expiration  |
		|	Visa		|	4111111111111111 | 1220  |
		|	Mastercard	|	5454545454545454 | 1230  |
		|	Discover	|	6011000993026909 | 1240  |
		|	Amex		|	371449635392376  | 1250  |
		
	
	# +++++++++++++++++++++ Negative Scenarios +++++++++++++++++++++
	
	@cardVaultBasic_byInvalidCardNumber
    Scenario: Verify Card Vauly by Invalid Card Number
        Given I have valid credentials
        And I have a valid request
        And I generate an authKey
        And I set card to { "number": "2222222222222222", "expiration": "1220" }
        When I POST to /api/vault/card
        Then response code should be 420
        And response body should be valid json       
		And response body path $.gatewayResponse.code should be 400000
		And response body path $.gatewayResponse.message should be There was a problem with the request. Please see 'detail' for more.
		And response body path $.gatewayResponse.detail should be INVALID CARDNUMBER
		
	@cardVaultBasic_byBlankCardNumber
    Scenario: Verify Card Vauly by Blank Card Number
        Given I have valid credentials
        And I have a valid request
        And I generate an authKey
        And I set card to {"number": "","expiration": "1220" }
        When I POST to /api/vault/card
        Then response code should be 400
        And response body should be valid json       
		And response body path $.gatewayResponse.code should be 100008
		And response body path $.gatewayResponse.message should be Invalid message request content
		And response body path $.gatewayResponse.detail should be `Required content: cardData(number,expiration) or account(routingNumber, accountNumber, type)`
		
	@cardVaultBasic_byWithoutCardNumberTag
    Scenario: Verify Card Vauly by Without Card Number Tag
        Given I have valid credentials
        And I have a valid request
        And I generate an authKey
        And I set card to {"expiration": "123" }
        When I POST to /api/vault/card
        Then response code should be 400
        And response body should be valid json       
		And response body path $.gatewayResponse.code should be 100008
		And response body path $.gatewayResponse.message should be Invalid message request content
		And response body path $.gatewayResponse.detail should be Required content: cardData
		
	@cardVaultBasic_byWithoutCardVaultData
    Scenario: Verify Card Vauly by Without Card Number Tag
        Given I have valid credentials
        And I have a valid request
        And I generate an authKey
        And I set card to {}
        When I POST to /api/vault/card
        Then response code should be 400
        And response body should be valid json       
		And response body path $.gatewayResponse.code should be 100008
		And response body path $.gatewayResponse.message should be Invalid message request content
		And response body path $.gatewayResponse.detail should be Required content: cardData
		
	@cardVaultBasic_byInvalidCardExpire
    Scenario: Verify Card Vauly by Invalid Card Expire
        Given I have valid credentials
        And I have a valid request
        And I generate an authKey
        And I set card to { "number": "4111111111111111", "expiration": "1100" }
        When I POST to /api/vault/card
        Then response code should be 404
        And response body should be valid json       
		And response body path $.gatewayResponse.code should be 100008
		And response body path $.gatewayResponse.message should be Invalid message request content
		And response body path $.gatewayResponse.detail should be Required content: cardData
	
	@cardVaultBasic_byBlankCardExpire
    Scenario: Verify Card Vauly by Blank Card Expire
        Given I have valid credentials
        And I have a valid request
        And I generate an authKey
        And I set card to { "number": "4111111111111111", "expiration": "" }
        When I POST to /api/vault/card
        Then response code should be 400
        And response body should be valid json       
		And response body path $.gatewayResponse.code should be 100008
		And response body path $.gatewayResponse.message should be Invalid message request content
		And response body path $.gatewayResponse.detail should be Required content: cardData
		
	@cardVaultBasic_byWithoutCardExpireTag
    Scenario: Verify Card Vauly by Without Card Expire Tag
        Given I have valid credentials
        And I have a valid request
        And I generate an authKey
        And I set card to { "number": "4111111111111111"}
        When I POST to /api/vault/card
        Then response code should be 400
        And response body should be valid json       
		And response body path $.gatewayResponse.code should be 100008
		And response body path $.gatewayResponse.message should be Invalid message request content
		And response body path $.gatewayResponse.detail should be Required content: cardData		
		
	@cardVaultCreate_byInvalidOperation
    Scenario: Verify Card Vauly by Invalid Operation
        Given I have valid credentials
        And I have a valid request
        And I set card to { "number": "5454545454545454", "expiration": "1220" }
        And I set vault to { "operation": "ABCD" }
        And I generate an authKey
        When I POST to /api/vault/card
        Then response code should be 404
        And response body should be valid json
        And response body path $.gatewayResponse.code should be 100006
		And response body path $.gatewayResponse.message should be Resource not found
		And response body path $.gatewayResponse.detail should be There is no resource at the path specified
		
	@cardVaultCreate_byBlankOperationValue
    Scenario: Verify Card Vauly by Blank Operation value
        Given I have valid credentials
        And I have a valid request
        And I set card to { "number": "5454545454545454", "expiration": "1220" }
        And I set vault to { "operation": ""}
        And I generate an authKey
        When I POST to /api/vault/card
        Then response code should be 401
        And response body should be valid json
        And response body path $.gatewayResponse.code should be 400000
		And response body path $.gatewayResponse.message should be There was a problem with the request. Please see 'detail' for more.
		And response body path $.gatewayResponse.detail should be `[{"field":"operation","expected":"","received":""}]`		
		
	@cardVaultUpdate_byInvalidOperation
    Scenario: create a vault token then update it by Invalid Operation
        Given I have valid credentials
        And I have a valid request
        And I set card to { "number": "5454545454545454", "expiration": "1220" }
        And I set vault to { "operation": "CREATE" }
        And I generate an authKey
        When I POST to /api/vault/card
        Then response code should be 200        
        And I store the vault token
        Given I have valid credentials
        And I have a valid request
        And I set card to { "number": "4111111111111111", "expiration": "0928" }
        And I set vault to { "operation": "ABCD" }
        And I grab a vault token
        And I generate an authKey
        When I POST to /api/vault/card
        Then response code should be 404
        And response body should be valid json
        And response body path $.gatewayResponse.code should be 100006
		And response body path $.gatewayResponse.message should be Resource not found
		And response body path $.gatewayResponse.detail should be There is no resource at the path specified
		
	@cardVaultUpdate_byBlankOperationValue
    Scenario: create a vault token then update it by Blank Operation Value
        Given I have valid credentials
        And I have a valid request
        And I set card to { "number": "5454545454545454", "expiration": "1220" }
        And I set vault to { "operation": "CREATE" }
        And I generate an authKey
        When I POST to /api/vault/card
        Then response code should be 200        
        And I store the vault token
        Given I have valid credentials
        And I have a valid request
        And I set card to { "number": "4111111111111111", "expiration": "0928" }
        And I set vault to { "operation": "" }
        And I grab a vault token
        And I generate an authKey
        When I POST to /api/vault/card
        Then response code should be 401
        And response body should be valid json
        And response body path $.gatewayResponse.code should be 400000
		And response body path $.gatewayResponse.message should be There was a problem with the request. Please see 'detail' for more.
		And response body path $.gatewayResponse.detail should be `[{"field":"operation","expected":"","received":""}]`
    
    @cardVaultUpdate404_WithInvalidToken
    Scenario: attempt to update non existent token
        Given I have valid credentials
        And I have a valid request
        And I set card to { "number": "5454545454545454", "expiration": "1220" }
        And I set vault to { "operation": "UPDATE", "token": "NOTAREALTOKENLUL" }
        And I generate an authKey
        When I POST to /api/vault/card
        Then response code should be 404
        And response body should be valid json
        And response body path $.code should be 100006
		And response body path $.message should be Resource not found
		And response body path $.detail should be UNABLE TO LOCATE
        And response body should contain info  

    @cardVaultDelete404_WithInvalidToken
    Scenario: attempt to delete nonexistent token
        Given I have valid credentials
        And I have a valid request
        And I set vault to { "operation": "DELETE", "token": "NOTAREALTOKENLUL" }
        And I generate an authKey
        When I POST to /api/vault/card
        Then response code should be 404
        And response body should be valid json
		And response body path $.code should be 100006
		And response body path $.message should be Resource not found
		And response body path $.detail should be UNABLE TO LOCATE
        And response body should contain info

		
		