@intg
@payment
@token
Feature: Token Payments
    As a PaymentsJS developer
    I want a functional endpoint
    So that the library consumers can run payments against vault tokens
    
    @cardTokenPaymentSale_Create
    Scenario Outline: credit card sale with a token
        Given I have valid credentials
        And I have a valid request
        And I set card to { "number": "<CardNumber>", "expiration": "<Expiration>" }
        And I set vault to { "operation": "CREATE" }
        And I generate an authKey
        When I POST to /api/vault/card
        Then response code should be 200
        And response body should be valid json
        And response body path $.gatewayResponse.vaultResponse.status should be 1
		And response body path $.gatewayResponse.vaultResponse.message should be SUCCESS 
		And response body path $.gatewayResponse.vaultResponse.data should be `token`
        And I store the vault token
        Given I have valid credentials
        And I have a valid request
        And I grab a token token
        And I generate an authKey
        When I POST to /api/payment/token
        Then response code should be 201
        And response body should be valid json
        And response body path $.gatewayResponse.status should be Approved
		And response body path $.gatewayResponse.message should be APPROVED 000001
		And response body path $.gatewayResponse.code should be 000001
		And response body path $.gatewayResponse.reference should be `reference`
	Examples:
		|	CardName	|	CardNumber		 | Expiration  |
		|	Visa		|	4111111111111111 | 1220  |
		|	Mastercard	|	5454545454545454 | 1230  |
		|	Discover	|	6011000993026909 | 1240  |
		|	Amex		|	371449635392376  | 1250  |
		
	@cardTokenPaymentSale_Update
    Scenario Outline: credit card sale with a token then update it
        Given I have valid credentials
        And I have a valid request
        And I set card to { "number": "<Create_CardNumber>", "expiration": "<Create_Expiration>" }
        And I set vault to { "operation": "CREATE" }
        And I generate an authKey
        When I POST to /api/vault/card
        Then response code should be 200
        And response body should be valid json
        And response body path $.gatewayResponse.vaultResponse.status should be 1
		And response body path $.gatewayResponse.vaultResponse.message should be SUCCESS 
		And response body path $.gatewayResponse.vaultResponse.data should be `token`
        And I store the vault token
        Given I have valid credentials
        And I have a valid request
		And I set card to { "number": <Update_CardNumber>, "expiration": <Update_Expiration> }
        And I set vault to { "operation": "UPDATE" }
        And I grab a token token
        And I generate an authKey
        When I POST to /api/payment/token
        Then response code should be 201
		And response body should be valid json
        And response body path $.gatewayResponse.status should be Approved
		And response body path $.gatewayResponse.message should be APPROVED 000001
		And response body path $.gatewayResponse.code should be 000001
		And response body path $.gatewayResponse.reference should be `reference`
	Examples:
		| Create_CardNumber  | Update_CardNumber  | Create_Expiration  | Update_Expiration  |
		|	4111111111111111 |	371449635392376	  | 1220  			   | 1260 |
		|	5454545454545454 |	6011000993026909  | 1230  			   | 1270 |
		|	6011000993026909 |	5454545454545454  | 1240 			   | 1280 |
		|	371449635392376  |	4111111111111111  | 1250  			   | 1290 |

    @cardTokenPaymentSale_Delete
    Scenario Outline: deleting a credit card token
        Given I have valid credentials
        And I have a valid request
        And I set card to { "number": "<CardNumber>", "expiration": "<Expiration>" }
        And I set vault to { "operation": "CREATE" }
        And I generate an authKey
        When I POST to /api/vault/card
        Then response code should be 200
        And response body should be valid json
        And response body path $.gatewayResponse.vaultResponse.status should be 1
		And response body path $.gatewayResponse.vaultResponse.message should be SUCCESS 
		And response body path $.gatewayResponse.vaultResponse.data should be `token`
        And I store the vault token
        Given I have valid credentials
        And I have a valid request
        And I set vault to { "operation": "DELETE" }
        And I grab a token token
        And I generate an authKey
        When I POST to /api/payment/token
        Then response code should be 200
        And response body should be valid json
        And response body path $.vaultResponse.status should be 1
		And response body path $.vaultResponse.message should be DELETED 
		And response body path $.vaultResponse.data should be `token`
	Examples:
		|	CardName	|	CardNumber		 | Expiration  |
		|	Visa		|	4111111111111111 | 1220  |
		|	Mastercard	|	5454545454545454 | 1230  |
		|	Discover	|	6011000993026909 | 1240  |
		|	Amex		|	371449635392376  | 1250  |

    @achTokenPaymentSale_Create
    Scenario Outline: ach sale with a token
        Given I have valid credentials
        And I have a valid request
        And I set ach to { "accountNumber": "12345678901234", "routingNumber": "056008849", "type": <accountType> }
        And I set vault to { "operation": "CREATE" }
        And I generate an authKey
        When I POST to /api/vault/ach
        Then response code should be 200
        And response body should be valid json
        And response body path $.gatewayResponse.vaultResponse.status should be 1
		And response body path $.gatewayResponse.vaultResponse.message should be SUCCESS 
		And response body path $.gatewayResponse.vaultResponse.data should be `token`
        And I store the vault token
        Given I have valid credentials
        And I have a valid request
        And I set token to { "secCode": <SecCode> }
		And I have valid customer data
        And I grab a token token
        And I generate an authKey
        And I have valid billing data
        When I POST to /api/payment/token
        Then response code should be 201
        And response body should be valid json
        And response body should contain orderNumber
		And response body path $.gatewayResponse.status should be Approved
		And response body path $.gatewayResponse.message should be ACCEPTED
		And response body path $.gatewayResponse.reference should be `reference`
  Examples:
	|SecCode|accountType|
	|"ARC"|"Savings"|
	|"ARC"|"Checking"|
	|"CCD"|"Savings"|
	|"CCD"|"Checking"|
	|"PPD"|"Savings"|
	|"PPD"|"Checking"|
	|"RCK"|"Savings"|
	|"RCK"|"Checking"|
	|"TEL"|"Savings"|
	|"TEL"|"Checking"|
	|"WEB"|"Savings"|
	|"WEB"|"Checking"|
	
	@achTokenPaymentSale_Update
    Scenario Outline: Create ach sale with a token and update it
        Given I have valid credentials
        And I have a valid request
        And I set ach to { "accountNumber": "12345678901234", "routingNumber": "056008849", "type": "Savings" }
        And I set vault to { "operation": "CREATE" }
        And I generate an authKey
        When I POST to /api/vault/ach
        Then response code should be 200
        And response body should be valid json
        And response body path $.gatewayResponse.vaultResponse.status should be 1
		And response body path $.gatewayResponse.vaultResponse.message should be SUCCESS 
		And response body path $.gatewayResponse.vaultResponse.data should be `token`
        And I store the vault token
        Given I have valid credentials
        And I have a valid request		
        And I set ach to { "accountNumber": "12345678901234", "routingNumber": "056008849", "type": <accountType> }
		And I set token to { "secCode": <SecCode> }
		And I set vault to { "operation": "UPDATE" }
		And I have valid customer data
        And I grab a token token
        And I generate an authKey
        And I have valid billing data
        When I POST to /api/payment/token
        Then response code should be 201
        And response body should be valid json
        And response body should contain orderNumber
		And response body path $.gatewayResponse.status should be Approved
		And response body path $.gatewayResponse.message should be ACCEPTED
		And response body path $.gatewayResponse.reference should be `reference`
  Examples:
	|SecCode|accountType|
	|"ARC"|"Savings"|
	|"ARC"|"Checking"|
	|"CCD"|"Savings"|
	|"CCD"|"Checking"|
	|"PPD"|"Savings"|
	|"PPD"|"Checking"|
	|"RCK"|"Savings"|
	|"RCK"|"Checking"|
	|"TEL"|"Savings"|
	|"TEL"|"Checking"|
	|"WEB"|"Savings"|
	|"WEB"|"Checking"|
	
    @achTokenPaymentSale_Delete
    Scenario Outline: deleting an ach token
        Given I have valid credentials
        And I have a valid request
        And I set ach to { "accountNumber": "12345678901234", "routingNumber": "056008849", "type": <accountType> }
        And I set vault to { "operation": "CREATE" }
        And I generate an authKey
        When I POST to /api/vault/ach
        Then response code should be 200
        And response body should be valid json
        And response body path $.gatewayResponse.vaultResponse.status should be 1
		And response body path $.gatewayResponse.vaultResponse.message should be SUCCESS 
		And response body path $.gatewayResponse.vaultResponse.data should be `token`
        And I store the vault token
        Given I have valid credentials
        And I have a valid request
        And I set vault to { "operation": "DELETE" }
        And I grab a token token
        And I generate an authKey
        When I POST to /api/payment/token
        Then response code should be 200
        And response body should be valid json
        And response body path $.vaultResponse.status should be 1
		And response body path $.vaultResponse.message should be DELETED 
		And response body path $.vaultResponse.data should be `token`
	 Examples:
		|accountType|
		|"Savings"  |
		|"Checking" |
		
	# ++++++++++++++ Negative Scenarios +++++++++++++++++++++++
	
	@cardTokenPaymentSale_byWithOutCardData
    Scenario: credit card sale with a token and WithOut CardData
        Given I have valid credentials
        And I have a valid request
        And I set card to {}
        And I set vault to { "operation": "CREATE" }
        And I generate an authKey
        When I POST to /api/vault/card
        Then response code should be 400
        And response body path $.gatewayResponse.code should be 100008
		And response body path $.gatewayResponse.message should be Invalid message request content
		And response body path $.gatewayResponse.detail should be Required content: cardData\(number,expiration\) or account\(routingNumber, accountNumber, type\)
        	
	@cardTokenPaymentSale_byBlankCardNumber
    Scenario: credit card sale with a token withOut Card Number  
        Given I have valid credentials
        And I have a valid request
        And I generate an authKey
		And I set card to {"expiration": "2018" }     
        And I set vault to { "operation": "CREATE" }
        And I generate an authKey
        When I POST to /api/vault/card
        And response body should be valid json
		Then response code should be 400
        And response body path $.gatewayResponse.code should be 100008
		And response body path $.gatewayResponse.message should be Invalid message request content
		And response body path $.gatewayResponse.detail should be Required content: cardData\(number,expiration\) or account\(routingNumber, accountNumber, type\)

	@cardTokenPaymentSale_byBlankExpireDate
    Scenario: credit card sale with a token withOut Expire Data 
        Given I have valid credentials
        And I have a valid request
        And I generate an authKey
		And I set card to { "number": "4111111111111111" }     
        And I set vault to { "operation": "CREATE" }
        And I generate an authKey
        When I POST to /api/vault/card
        Then response code should be 400
        And response body should be valid json
        And response body path $.gatewayResponse.code should be 100008
		And response body path $.gatewayResponse.message should be Invalid message request content
		And response body path $.gatewayResponse.detail should be Required content: cardData\(number,expiration\) or account\(routingNumber, accountNumber, type\)

	@cardTokenPaymentSale_byInvalidCardNumber
    Scenario: credit card sale with a token by Invalid CardNumber
        Given I have valid credentials
        And I have a valid request
        And I set card to { "number": "9111111911111119", "expiration": "1220" }
        And I set vault to { "operation": "CREATE" }
        And I generate an authKey
        When I POST to /api/vault/card
        Then response code should be 404
        And response body should be valid json
		And response body path $.gatewayResponse.code should be 400000
		And response body path $.gatewayResponse.message should be There was a problem with the request. Please see 'detail' for more. 
		And response body path $.gatewayResponse.detail should be INVALID CARDNUMBER

	@cardTokenPaymentSale_byInvalidOperation
    Scenario: credit card sale with a token by Invalid CardNumber
        Given I have valid credentials
        And I have a valid request
        And I set card to { "number": "4111111111111111", "expiration": "1220" }
        And I set vault to { "operation": "Test" }
        And I generate an authKey
        When I POST to /api/vault/card
        Then response code should be 404
        And response body should be valid json
		And response body path $.gatewayResponse.code should be 100006
		And response body path $.gatewayResponse.message should be Resource not found
		And response body path $.gatewayResponse.detail should be There is no resource at the path specified
		
	@cardTokenPaymentUpdate_byInvalidCreateOperation
    Scenario: credit card sale with a token by Invalid Operation
        Given I have valid credentials
        And I have a valid request
        And I set card to { "number": "4111111111111111", "expiration": "1220" }
        And I set vault to { "operation": "CREATE" }
        And I generate an authKey
        When I POST to /api/vault/card
        Then response code should be 200
        And response body should be valid json
        And response body path $.gatewayResponse.vaultResponse.status should be 1
		And response body path $.gatewayResponse.vaultResponse.message should be SUCCESS 
		And response body path $.gatewayResponse.vaultResponse.data should be `token`
        And I store the vault token
        Given I have valid credentials
        And I have a valid request
		And I set card to { "number": <Update_CardNumber>, "expiration": <Update_Expiration> }
        And I set vault to { "operation": "Test" }
        And I grab a token token
        And I generate an authKey
        When I POST to /api/payment/token
        Then response code should be 404
        And response body should be valid json
		And response body path $.gatewayResponse.code should be 100006
		And response body path $.gatewayResponse.message should be Resource not found
		And response body path $.gatewayResponse.detail should be There is no resource at the path specified

	@cardTokenDelete_byInvalidCreateOperation
    Scenario: deleting a credit card token by Invalid Operation
        Given I have valid credentials
        And I have a valid request
        And I set card to { "number": "4111111111111111", "expiration": "1220" }
        And I set vault to { "operation": "CREATE" }
        And I generate an authKey
        When I POST to /api/vault/card
        Then response code should be 200
        And response body should be valid json
        And response body path $.gatewayResponse.vaultResponse.status should be 1
		And response body path $.gatewayResponse.vaultResponse.message should be SUCCESS 
		And response body path $.gatewayResponse.vaultResponse.data should be `token`
        And I store the vault token
        Given I have valid credentials
        And I have a valid request
        And I set vault to { "operation": "Test" }
        And I grab a token token
        And I generate an authKey
        When I POST to /api/payment/token
        Then response code should be 404
        And response body should be valid json
		And response body path $.gatewayResponse.code should be 100006
		And response body path $.gatewayResponse.message should be Resource not found
		And response body path $.gatewayResponse.detail should be There is no resource at the path specified

	@achTokenPaymentSale_byWithOutACHdata
    Scenario: ach sale with a token by WithOut ACH data
        Given I have valid credentials
        And I have a valid request
        And I set ach to {}
        And I set vault to { "operation": "CREATE" }
        And I generate an authKey
        When I POST to /api/vault/ach
        Then response code should be 400
        And response body path $.gatewayResponse.code should be 100008
		And response body path $.gatewayResponse.message should be Invalid message request content
		And response body path $.gatewayResponse.detail should be Required content: account\(routingNumber, accountNumber, type\)
		
	@achTokenPaymentSale_byWithOutAccountNumber
    Scenario: ach sale with a token by WithOut AccountNumber data
        Given I have valid credentials
        And I have a valid request
        And I set ach to { "routingNumber": "056008849", "type": "Savings" }
        And I set vault to { "operation": "CREATE" }
        And I generate an authKey
        When I POST to /api/vault/ach
        Then response code should be 400
        And response body path $.gatewayResponse.code should be 100008
		And response body path $.gatewayResponse.message should be Invalid message request content
		And response body path $.gatewayResponse.detail should be Required content: account\(routingNumber, accountNumber, type\)
        	
	@achTokenPaymentSale_byWithOutRoutingNumber
    Scenario: ach sale with a token by WithOut RoutingNumber data
        Given I have valid credentials
        And I have a valid request
        And I set ach to { "accountNumber": "12345678901234", "type": "Savings"  }
        And I set vault to { "operation": "CREATE" }
        And I generate an authKey
        When I POST to /api/vault/ach
        Then response code should be 400
        And response body path $.gatewayResponse.code should be 100008
		And response body path $.gatewayResponse.message should be Invalid message request content
		And response body path $.gatewayResponse.detail should be Required content: account\(routingNumber, accountNumber, type\)
        
	@achTokenPaymentSale_byWithOutAccountType
    Scenario: ach sale with a token by WithOut AccountType data
        Given I have valid credentials
        And I have a valid request
        And I set ach to { "accountNumber": "12345678901234", "routingNumber": "056008849" }
        And I set vault to { "operation": "CREATE" }
        And I generate an authKey
        When I POST to /api/vault/ach
        Then response code should be 400
        And response body path $.gatewayResponse.code should be 100008
		And response body path $.gatewayResponse.message should be Invalid message request content
		And response body path $.gatewayResponse.detail should be Required content: account\(routingNumber, accountNumber, type\)
		
	@achTokenPaymentSale_byInvalidAccountType
    Scenario: ach sale with a token by Invalid AccountType
        Given I have valid credentials
        And I have a valid request
        And I set ach to { "accountNumber": "12345678901234", "routingNumber": "056008849", "type": "Test" }
        And I set vault to { "operation": "CREATE" }
        And I generate an authKey
        When I POST to /api/vault/ach
        Then response code should be 404
        And response body should be valid json
		And response body path $.gatewayResponse.code should be 100006
		And response body path $.gatewayResponse.message should be Resource not found
		And response body path $.gatewayResponse.detail should be There is no resource at the path specified
		
	@achTokenPaymentSale_byInvalidAccountNumber
    Scenario: ach sale with a token by Invalid AccountNumber
        Given I have valid credentials
        And I have a valid request
        And I set ach to { "accountNumber": "99999999999999", "routingNumber": "056008849", "type": "Savings" }
        And I set vault to { "operation": "CREATE" }
        And I generate an authKey
        When I POST to /api/vault/ach
        Then response code should be 404
        And response body should be valid json
		And response body path $.gatewayResponse.code should be 100006
		And response body path $.gatewayResponse.message should be Resource not found
		And response body path $.gatewayResponse.detail should be There is no resource at the path specified
		
	@achTokenPaymentSale_byInvalidRoutingNumber
    Scenario: ach sale with a token by Invalid RoutingNumber
        Given I have valid credentials
        And I have a valid request
        And I set ach to { "accountNumber": "12345678901234", "routingNumber": "999999999", "type": "Savings" }
        And I set vault to { "operation": "CREATE" }
        And I generate an authKey
        When I POST to /api/vault/ach
        Then response code should be 404
        And response body should be valid json
		And response body path $.gatewayResponse.code should be 100006
		And response body path $.gatewayResponse.message should be Resource not found
		And response body path $.gatewayResponse.detail should be There is no resource at the path specified

	@achTokenPaymentSale_byInvalidOperation
    Scenario: ach sale with a token by Invalid Operation
        Given I have valid credentials
        And I have a valid request
        And I set ach to { "accountNumber": "12345678901234", "routingNumber": "056008849", "type": "Savings" }
        And I set vault to { "operation": "Test" }
        And I generate an authKey
        When I POST to /api/vault/ach
        Then response code should be 404
        And response body should be valid json
		And response body path $.gatewayResponse.code should be 100006
		And response body path $.gatewayResponse.message should be Resource not found
		And response body path $.gatewayResponse.detail should be There is no resource at the path specified
		
	@achTokenPaymentSaleUpdate_byInvDeleteOperation
    Scenario: Create ach sale with a token by Invalid Operation
        Given I have valid credentials
        And I have a valid request
        And I set ach to { "accountNumber": "12345678901234", "routingNumber": "056008849", "type": "Savings" }
        And I set vault to { "operation": "CREATE" }
        And I generate an authKey
        When I POST to /api/vault/ach
        Then response code should be 200
        And response body should be valid json
        And response body path $.gatewayResponse.vaultResponse.status should be 1
		And response body path $.gatewayResponse.vaultResponse.message should be SUCCESS 
		And response body path $.gatewayResponse.vaultResponse.data should be `token`
        And I store the vault token
        Given I have valid credentials
        And I have a valid request		
        And I set ach to { "accountNumber": "12345678901234", "routingNumber": "056008849", "type": "Savings" }
		And I set token to { "secCode": "PPD" }
		And I set vault to { "operation": "Test" }
		And I have valid customer data
        And I grab a token token
        And I generate an authKey
        And I have valid billing data
        When I POST to /api/payment/token
        Then response code should be 404
        And response body should be valid json
		And response body path $.gatewayResponse.code should be 100006
		And response body path $.gatewayResponse.message should be Resource not found
		And response body path $.gatewayResponse.detail should be There is no resource at the path specified
		
	@achTokenPaymentDelete_byInvDeleteOperation
    Scenario: deleting an ach token by Invalid Operation
        Given I have valid credentials
        And I have a valid request
        And I set ach to { "accountNumber": "12345678901234", "routingNumber": "056008849", "type": "Savings" }
        And I set vault to { "operation": "CREATE" }
        And I generate an authKey
        When I POST to /api/vault/ach
        Then response code should be 200
        And response body should be valid json
        And response body path $.gatewayResponse.vaultResponse.status should be 1
		And response body path $.gatewayResponse.vaultResponse.message should be SUCCESS 
		And response body path $.gatewayResponse.vaultResponse.data should be `token`
        And I store the vault token
        Given I have valid credentials
        And I have a valid request
		And I set token to { "secCode": "RCK" }
        And I set vault to { "operation": "Test" }
        And I grab a token token
        And I generate an authKey
		And I have valid billing data
        When I POST to /api/payment/token
        Then response code should be 404
        And response body should be valid json
		And response body path $.gatewayResponse.code should be 100006
		And response body path $.gatewayResponse.message should be Resource not found
		And response body path $.gatewayResponse.detail should be There is no resource at the path specified
		
	
		