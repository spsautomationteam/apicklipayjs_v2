@intg
@payment
@ach
Feature: ACH Payments
    As a PaymentsJS developer
    I want a functional endpoint
    So that the library consumers can run ach payments
    
    @achPaymentSale
    Scenario: basic sale transaction
        Given I have valid credentials
        And I have a valid request
        And I generate an authKey
        And I set ach to { "accountNumber": "12345678901234", "routingNumber": "056008849", "type": "Checking", "secCode": "CCD" }
        And I have valid billing data
		And I have valid customer data
        When I POST to /api/payment/ach
        Then response code should be 201
        And response body should be valid json
        And response body should contain ACCEPTED
		
	@achPaymentSale_Parameterize_BySecCodeAndType
    Scenario Outline: basic ACH transaction by SecCode and Account Type
        Given I have valid credentials
        And I have a valid request
        And I generate an authKey
	#	And I set ach to { "transactionClass": "PPD", "amounts":{ "total": 1.0 }, "account":{ "type": "Checking", "routingNumber": "056008849", "accountNumber": "12345678901234" }, "customer": { "dateOfBirth": "1998-01-01", "ssn": "123456789","license": {"number": "1234567","stateCode": "VA"},"ein": "123456789", "email": "test@gmail.com","telephone": "001111111111", "fax": "001111111"  },"billing": { "name": { "first": "jean-luc", "last": "picard" }, "address": "123 Street Rd", "city": "cityville", "state": "va", "postalCode": "12345" } }
        And I set ach to { "accountNumber": "12345678901234", "routingNumber": "056008849", "type": <accountType>, "secCode": <SecCode> }
        And I have valid billing data
		And I have valid customer data
        When I POST to /api/payment/ach
        Then response code should be 201
        And response body should be valid json
		And response body path $.gatewayResponse.status should be Approved
		And response body path $.gatewayResponse.message should be ACCEPTED 
		And response body path $.gatewayResponse.reference should be `reference`
		And response body should contain orderNumber		
   Examples:
	|SecCode|accountType|
	|"ARC"|"Savings"|
	|"ARC"|"Checking"|
	|"CCD"|"Savings"|
	|"CCD"|"Checking"|
	|"PPD"|"Savings"|
	|"PPD"|"Checking"|
	|"RCK"|"Savings"|
	|"RCK"|"Checking"|
	|"TEL"|"Savings"|
	|"TEL"|"Checking"|
	|"WEB"|"Savings"|
	|"WEB"|"Checking"|			
		
		# ++++++++++++++ Negative Scenarios +++++++++++++++++++++++	
		
	@achPayment_byInvalidAccountType
    Scenario: basic Ach transaction by Invalid Account Type 
        Given I have valid credentials
        And I have a valid request
        And I generate an authKey
        And I set ach to { "accountNumber": "12345678901234", "routingNumber": "056008849", "type": "Test", "secCode": "PPD" }
        And I have valid billing data
        When I POST to /api/payment/ach
        Then response code should be 404
        And response body should be valid json
        And response body path $.gatewayResponse.code should be 400000
		And response body path $.gatewayResponse.message should be There was a problem with the request. Please see 'detail' for more. 
		And response body path $.gatewayResponse.detail should be The Type field is required.; Error converting value "Test" to type

	@achPayment_byInvalidSecCode
    Scenario: basic Ach transaction by Invalid SecCode
        Given I have valid credentials
        And I have a valid request
        And I generate an authKey
        And I set ach to { "accountNumber": "12345678901234", "routingNumber": "056008849", "type": "Checking", "secCode": "ABCD" }
        And I have valid billing data
        When I POST to /api/payment/ach
        Then response code should be 404
        And response body should be valid json
        And response body path $.gatewayResponse.code should be 400000
		And response body path $.gatewayResponse.message should be There was a problem with the request. Please see 'detail' for more. 
		And response body path $.gatewayResponse.detail should be SecCode is required; Error converting value "ABCD" to type	
		
		@achPayment_byWithOutAccountNumberTag
    Scenario: basic Ach transaction by Invalid Account Number tag
        Given I have valid credentials
        And I have a valid request
        And I generate an authKey
        And I set ach to { "routingNumber": "056008849", "type": "Checking", "secCode": "PPD" }
        And I have valid billing data
        When I POST to /api/payment/ach
        Then response code should be 400
        And response body should be valid json
        And response body path $.gatewayResponse.code should be 400000
		And response body path $.gatewayResponse.message should be There was a problem with the request. Please see 'detail' for more. 
		And response body path $.gatewayResponse.detail should be The AccountNumber field is required.		
		
	@achPayment_byWithOutRoutingNumberTag
    Scenario: basic Ach transaction by WithOut Routing Number tag
        Given I have valid credentials
        And I have a valid request
        And I generate an authKey
        And I set ach to {  "accountNumber": "12345678901234",  "type": "Checking", "secCode": "PPD" }
        And I have valid billing data
        When I POST to /api/payment/ach
        Then response code should be 400
        And response body should be valid json
        And response body path $.gatewayResponse.code should be 400000
		And response body path $.gatewayResponse.message should be There was a problem with the request. Please see 'detail' for more. 
		And response body path $.gatewayResponse.detail should be The RoutingNumber field is required.
		
	@achPayment_byWithOutAccountTypeTag
    Scenario: basic Ach transaction by WithOut Account Type tag
        Given I have valid credentials
        And I have a valid request
        And I generate an authKey
        And I set ach to {  "accountNumber": "12345678901234",  "routingNumber": "056008849", "secCode": "PPD" }
        And I have valid billing data
        When I POST to /api/payment/ach
        Then response code should be 400
        And response body should be valid json
        And response body path $.gatewayResponse.code should be 400000
		And response body path $.gatewayResponse.message should be There was a problem with the request. Please see 'detail' for more. 
		And response body path $.gatewayResponse.detail should be The Type field is required.
		
	@achPayment_byWithOutSecCodeTag
    Scenario: basic Ach transaction by WithOut Sec Code tag
        Given I have valid credentials
        And I have a valid request
        And I generate an authKey
        And I set ach to {  "accountNumber": "12345678901234",  "routingNumber": "056008849", "type": "Checking"}
        And I have valid billing data
        When I POST to /api/payment/ach
        Then response code should be 400
        And response body should be valid json
        And response body path $.gatewayResponse.code should be 400000
		And response body path $.gatewayResponse.message should be There was a problem with the request. Please see 'detail' for more. 
		And response body path $.gatewayResponse.detail should be SecCode is required
		
	@achPayment_byBlankACHData
    Scenario: basic Ach transaction by Blank ACH Data
        Given I have valid credentials
        And I have a valid request
        And I generate an authKey
        And I set ach to {}
        And I have valid billing data
        When I POST to /api/payment/ach
        Then response code should be 400
        And response body should be valid json
        And response body path $.gatewayResponse.code should be 400000
		And response body path $.gatewayResponse.message should be There was a problem with the request. Please see 'detail' for more. 
		And response body path $.gatewayResponse.detail should be InvalidRequestData : request.Account: The Type field is required.; request.Account: The RoutingNumber field is required.; request.Account: The AccountNumber field is required.
		
		
		

	@achPayment_byWithOutBillingData
    Scenario: basic Ach transaction by WithOut Billing Data
        Given I have valid credentials
        And I have a valid request
        And I generate an authKey
        And I set ach to { "accountNumber": "12345678901234", "routingNumber": "056008849", "type": "Checking", "secCode": "PPD" }
        When I POST to /api/payment/ach
        Then response code should be 400
        And response body should be valid json
        And response body path $.gatewayResponse.code should be 400000
		And response body path $.gatewayResponse.message should be There was a problem with the request. Please see 'detail' for more. 
		And response body path $.gatewayResponse.detail should be The Billing field is required.

	@achPayment_byWithOutCustomerData
    Scenario: basic Ach transaction by WithOut Billing Data
        Given I have valid credentials
        And I have a valid request
        And I generate an authKey
        And I set ach to { "accountNumber": "12345678901234", "routingNumber": "056008849", "type": "Checking", "secCode": "CCD" }
		And I have valid billing data
        When I POST to /api/payment/ach
        Then response code should be 400
        And response body should be valid json
        And response body path $.gatewayResponse.code should be 400000
		And response body path $.gatewayResponse.message should be There was a problem with the request. Please see 'detail' for more. 
		And response body path $.gatewayResponse.detail should be Customer is required	

	@achPayment_CustomerInvData_byWithOutLicenseNumber
    Scenario: basic ACH Payment transaction with Invalid Customer Data(WithOutLicenseNumber)
        Given I have valid credentials
        And I have a valid request
        And I generate an authKey
        And I set ach to { "accountNumber": "12345678901234", "routingNumber": "056008849", "type": "Checking", "secCode": "CCD" }
        And I have valid billing data
		And I have Invalid customer data with Body { "dateOfBirth": "1998-01-01", "ssn": "123456789","license": {"stateCode": "VA"},"ein": "123456789", "email": "test@gmail.com","telephone": "001111111111", "fax": "001111111"  }
		When I POST to /api/payment/ach
        Then response code should be 400
        And response body should be valid json
        And response body path $.gatewayResponse.code should be 400000
		And response body path $.gatewayResponse.message should be There was a problem with the request. Please see 'detail' for more. 
		And response body path $.gatewayResponse.detail should be The Number field is required.	
		
	@achPayment_CustomerInvData_byWithOutStateCode
    Scenario: basic ACH Payment transaction with Invalid Customer Data(WithOutStateCode)
        Given I have valid credentials
        And I have a valid request
        And I generate an authKey
        And I set ach to { "accountNumber": "12345678901234", "routingNumber": "056008849", "type": "Checking", "secCode": "CCD" }
        And I have valid billing data
		And I have Invalid customer data with Body { "dateOfBirth": "1998-01-01", "ssn": "123456789","license": {"number": "1234567"},"ein": "123456789", "email": "test@gmail.com","telephone": "001111111111", "fax": "001111111"  }
		When I POST to /api/payment/ach
        Then response code should be 400
        And response body should be valid json
        And response body path $.gatewayResponse.code should be 400000
		And response body path $.gatewayResponse.message should be There was a problem with the request. Please see 'detail' for more. 
		And response body path $.gatewayResponse.detail should be The StateCode field is required.
		
	@achPayment_CustomerInvData_byWithOutEIN
    Scenario: basic ACH Payment transaction with Invalid Customer Data(WithOutEIN)
        Given I have valid credentials
        And I have a valid request
        And I generate an authKey
        And I set ach to { "accountNumber": "12345678901234", "routingNumber": "056008849", "type": "Checking", "secCode": "CCD" }
        And I have valid billing data
		And I have Invalid customer data with Body { "dateOfBirth": "1998-01-01", "ssn": "123456789","license": {"number": "1234567", "stateCode": "VA"},"email": "test@gmail.com","telephone": "001111111111", "fax": "001111111"  }
		When I POST to /api/payment/ach
        Then response code should be 400
        And response body should be valid json
        And response body path $.gatewayResponse.code should be 400000
		And response body path $.gatewayResponse.message should be There was a problem with the request. Please see 'detail' for more. 
		And response body path $.gatewayResponse.detail should be EIN is required
		
	@achPayment_BillingInvData_byWithOutFirstTag
    Scenario: basic ACH Payment transaction with Invalid Billing Data(WithOutFirstTag)
        Given I have valid credentials
        And I have a valid request
        And I generate an authKey
        And I set ach to { "accountNumber": "12345678901234", "routingNumber": "056008849", "type": "Checking", "secCode": "CCD" }
        And I have Invalid billing data with Body { "name": {"middle": "Q", "last": "Payments", "suffix": "JS"},"address": "123 Address St","city": "Cityville","state": "VA","postalCode": "12345","country": "USA"}
		And I have valid customer data
		When I POST to /api/payment/ach
        Then response code should be 400
        And response body should be valid json
        And response body path $.gatewayResponse.code should be 400000
		And response body path $.gatewayResponse.message should be There was a problem with the request. Please see 'detail' for more. 
		And response body path $.gatewayResponse.detail should be The First field is required.
		
	@achPayment_BillingInvData_byWithOutLastTag
    Scenario: basic ACH Payment transaction with Invalid Billing Data(WithOutLastTag)
        Given I have valid credentials
        And I have a valid request
        And I generate an authKey
        And I set ach to { "accountNumber": "12345678901234", "routingNumber": "056008849", "type": "Checking", "secCode": "CCD" }
        And I have Invalid billing data with Body { "name": {"first": "Apickli", "middle": "Q", "suffix": "JS"},"address": "123 Address St","city": "Cityville","state": "VA","postalCode": "12345","country": "USA"}
		And I have valid customer data
		When I POST to /api/payment/ach
        Then response code should be 400
        And response body should be valid json
        And response body path $.gatewayResponse.code should be 400000
		And response body path $.gatewayResponse.message should be There was a problem with the request. Please see 'detail' for more. 
		And response body path $.gatewayResponse.detail should be The Last field is required.
		
	@achPayment_BillingInvData_byWithOutAddressTag
    Scenario: basic ACH Payment transaction with Invalid Billing Data(WithOutAddressTag)
        Given I have valid credentials
        And I have a valid request
        And I generate an authKey
        And I set ach to { "accountNumber": "12345678901234", "routingNumber": "056008849", "type": "Checking", "secCode": "CCD" }
        And I have Invalid billing data with Body { "name": {"first": "Apickli", "middle": "Q", "last": "Payments", "suffix": "JS"}, "city": "Cityville","state": "VA","postalCode": "12345","country": "USA"}
		And I have valid customer data
		When I POST to /api/payment/ach
        Then response code should be 400
        And response body should be valid json
        And response body path $.gatewayResponse.code should be 400000
		And response body path $.gatewayResponse.message should be There was a problem with the request. Please see 'detail' for more. 
		And response body path $.gatewayResponse.detail should be Address is required
		
	@achPayment_BillingInvData_byWithOutCityTag
    Scenario: basic ACH Payment transaction with Invalid Billing Data(WithOutCityTag)
        Given I have valid credentials
        And I have a valid request
        And I generate an authKey
        And I set ach to { "accountNumber": "12345678901234", "routingNumber": "056008849", "type": "Checking", "secCode": "CCD" }
        And I have Invalid billing data with Body { "name": {"first": "Apickli", "middle": "Q", "last": "Payments", "suffix": "JS"},"address": "123 Address St","state": "VA","postalCode": "12345","country": "USA"}
		And I have valid customer data
		When I POST to /api/payment/ach
        Then response code should be 400
        And response body should be valid json
        And response body path $.gatewayResponse.code should be 400000
		And response body path $.gatewayResponse.message should be There was a problem with the request. Please see 'detail' for more. 
		And response body path $.gatewayResponse.detail should be City is required
		
	@achPayment_BillingInvData_byWithOutStateTag
    Scenario: basic ACH Payment transaction with Invalid Billing Data(WithOutStateTag)
        Given I have valid credentials
        And I have a valid request
        And I generate an authKey
        And I set ach to { "accountNumber": "12345678901234", "routingNumber": "056008849", "type": "Checking", "secCode": "CCD" }
        And I have Invalid billing data with Body { "name": {"first": "Apickli", "middle": "Q", "last": "Payments", "suffix": "JS"},"address": "123 Address St","city": "Cityville","postalCode": "12345","country": "USA"}
		And I have valid customer data
		When I POST to /api/payment/ach
        Then response code should be 400
        And response body should be valid json
        And response body path $.gatewayResponse.code should be 400000
		And response body path $.gatewayResponse.message should be There was a problem with the request. Please see 'detail' for more. 
		And response body path $.gatewayResponse.detail should be State is required
		
	@achPayment_BillingInvData_byWithOutPostalCodeTag
    Scenario: basic ACH Payment transaction with Invalid Billing Data(WithOutPostalCodeTag)
        Given I have valid credentials
        And I have a valid request
        And I generate an authKey
        And I set ach to { "accountNumber": "12345678901234", "routingNumber": "056008849", "type": "Checking", "secCode": "CCD" }
        And I have Invalid billing data with Body { "name": {"first": "Apickli", "middle": "Q", "last": "Payments", "suffix": "JS"},"address": "123 Address St","city": "Cityville","state": "VA","country": "USA"}
		And I have valid customer data
		When I POST to /api/payment/ach
        Then response code should be 400
        And response body should be valid json
        And response body path $.gatewayResponse.code should be 400000
		And response body path $.gatewayResponse.message should be There was a problem with the request. Please see 'detail' for more. 
		And response body path $.gatewayResponse.detail should be PostalCode is required
		
	@achPayment_BillingInvData_byWithOutNameDetails
    Scenario: basic ACH Payment transaction with Invalid Billing Data(WithOutNameDetails)
        Given I have valid credentials
        And I have a valid request
        And I generate an authKey
        And I set ach to { "accountNumber": "12345678901234", "routingNumber": "056008849", "type": "Checking", "secCode": "CCD" }
        And I have Invalid billing data with Body { "name": { },"address": "123 Address St","city": "Cityville","state": "VA","postalCode": "12345","country": "USA"}
		And I have valid customer data
		When I POST to /api/payment/ach
        Then response code should be 400
        And response body should be valid json
        And response body path $.gatewayResponse.code should be 400000
		And response body path $.gatewayResponse.message should be There was a problem with the request. Please see 'detail' for more. 
		And response body path $.gatewayResponse.detail should be request.Billing.Name: The First field is required.; request.Billing.Name: The Last field is required.
		
	@achPayment_BillingInvData_byWithOutAddressDetails
    Scenario: basic ACH Payment transaction with Invalid Billing Data(WithOutAddressDetails)
        Given I have valid credentials
        And I have a valid request
        And I generate an authKey
        And I set ach to { "accountNumber": "12345678901234", "routingNumber": "056008849", "type": "Checking", "secCode": "CCD" }
        And I have Invalid billing data with Body { "name": {"first": "Apickli", "middle": "Q", "last": "Payments", "suffix": "JS"}}
		And I have valid customer data
		When I POST to /api/payment/ach
        Then response code should be 400
        And response body should be valid json
        And response body path $.gatewayResponse.code should be 400000
		And response body path $.gatewayResponse.message should be There was a problem with the request. Please see 'detail' for more. 
		And response body path $.gatewayResponse.detail should be request.Billing: Address is required; request.Billing: City is required; request.Billing: State is required; request.Billing: PostalCode is required
		
	