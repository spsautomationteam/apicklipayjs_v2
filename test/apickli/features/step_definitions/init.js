/* jshint node:true */
'use strict';

var factory = require('./factory.js');
var config = require('../../config/config.json');

var invalidClientId = config.paymentsjs.invalidClientId;
var clientId = config.paymentsjs.clientId;
var clientSecret = config.paymentsjs.clientSecret;
var merchantId = config.paymentsjs.merchantId;
var merchantKey = config.paymentsjs.merchantKey;

module.exports = function() {
    // cleanup before every scenario
    this.Before(function(scenario, callback) {
        this.apickli = factory.getNewApickliInstance();
        this.apickli.storeValueInScenarioScope("invalidClientId", invalidClientId);
        this.apickli.storeValueInScenarioScope("clientId", clientId);
        this.apickli.storeValueInScenarioScope("clientSecret", clientSecret);
		this.apickli.storeValueInScenarioScope("merchantId", merchantId);
        this.apickli.storeValueInScenarioScope("merchantKey", merchantKey);
        callback();
    });
};

