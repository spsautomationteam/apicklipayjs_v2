/* jslint node: true */
'use strict';

// Create a new singleton
var hmacTools = new (require("../../../hmacTools.js"))();
var async = require('async');

module.exports = function () {

    var vaultTokens = [];
    var tokenInUse = '';
    
    this.Given(/^I have valid credentials$/, function (callback) {
        // Already set by config and init.js
        this.apickli.headers['clientId'] = this.apickli.scenarioVariables.clientId;
        callback();
    });
    
    this.Given(/^I have valid billing data$/, function (callback) {
        this.apickli.scenarioVariables.currentRequest["billing"] = {
            "name": {
                "first": "Apickli",
                "middle": "Q",
                "last": "Payments",
                "suffix": "JS"
            },
            "address": "123 Address St",
            "city": "Cityville",
            "state": "VA",
            "postalCode": "12345",
            "country": "USA"
        };
        callback()
    });	
	
	this.Given(/^I have valid customer data$/, function (callback) {
        this.apickli.scenarioVariables.currentRequest["customer"] = {            
		   "dateOfBirth": "1998-01-01", 
		   "ssn": "123456789",
		   "license": {
		   "number": "1234567",
		   "stateCode": "VA"
		   },
		   "ein": "123456789", 
		   "email": "test@gmail.com",
		   "telephone": "001111111111", 
		   "fax": "001111111"
        };
        callback()
    });	
	
	this.Given(/^I have Invalid customer data with Body (.+)$/, function (data, callback) {
		let newData = data;
        try {
            newData = JSON.parse(data);
        }
        catch (ex) {
            newData = data;
        }
		this.apickli.scenarioVariables.currentRequest["customer"] = newData;
        callback()
    });	

	this.Given(/^I have Invalid billing data with Body (.+)$/, function (data, callback) {
		let newData = data;
        try {
            newData = JSON.parse(data);
        }
        catch (ex) {
            newData = data;
        }
		this.apickli.scenarioVariables.currentRequest["billing"] = newData;
        callback()
    });	
	

    this.Given(/^I have a valid request$/, function (callback) {
        
        var nonces = hmacTools.secureNonce(16);
        this.apickli.scenarioVariables.nonces = nonces;
        this.apickli.scenarioVariables.currentRequest = {
            auth: {
                clientId: this.apickli.scenarioVariables.clientId,
                authKey: void 0,
                merchantId: this.apickli.scenarioVariables.merchantId,
                merchantKey: this.apickli.scenarioVariables.merchantKey,
                salt: nonces.salt,
                requestId: "Invoice" + hmacTools.nonce(3)
            },
            payment: {
                totalAmount: "1.00"
            },
        }
        callback();
    });
    
    this.Given(/^I store the vault token$/, function (callback) {
        
        var token = JSON.parse(this.apickli.getResponseObject().body).gatewayResponse.vaultResponse.data;
        vaultTokens.push(token);
        //console.log(`    Tokens: ${vaultTokens}`)
        callback();
    });

    this.Given(/^I grab a vault token$/, function(callback) {
        tokenInUse = vaultTokens.pop();
        //console.log(`    Token: ${tokenInUse}`)
        this.apickli.scenarioVariables.currentRequest.vault = this.apickli.scenarioVariables.currentRequest.vault || {};
        this.apickli.scenarioVariables.currentRequest.vault["token"] = tokenInUse;
        callback();
    });

    this.Given(/^I grab a token token$/, function(callback) {
        tokenInUse = vaultTokens.pop();
        //console.log(`    Token: ${tokenInUse}`)
        this.apickli.scenarioVariables.currentRequest.token = this.apickli.scenarioVariables.currentRequest.token || {};
        this.apickli.scenarioVariables.currentRequest.token["token"] = tokenInUse;
        callback();
    });

    this.Then(/^the vault token should match$/, function(callback) {
        let back = JSON.parse(this.apickli.getResponseObject().body).vaultResponse.data
        let sent = tokenInUse;
        tokenInUse = null;
        console.log(`    Sent Out: ${sent}`)
        console.log(`    Got Back: ${back}`)
        if (sent != back) {
            throw('Vault token mismatch!');
        }
        callback();
    });

    this.Given(/^I set (.*) to (.+)$/, function(target, value, callback){
        let newVal = value;
        try {
            newVal = JSON.parse(value);
        }
        catch (ex) {
            newVal = value;
        }
        this.apickli.scenarioVariables.currentRequest[target] = newVal;
        callback();
    });
    
    this.When(/^I generate an authKey$/, function(callback){
        
        var authKey = hmacTools.encrypt(
            this.apickli.scenarioVariables.clientSecret,
            this.apickli.scenarioVariables.nonces.iv,
            this.apickli.scenarioVariables.nonces.salt,
            JSON.stringify(this.apickli.scenarioVariables.currentRequest)
        );
        this.apickli.headers["authKey"] = authKey;
        delete this.apickli.scenarioVariables.currentRequest.auth.merchantKey;
        callback();
    })

    this.When(/^I POST to (.*)$/, function (pathSuffix, callback) {
        this.apickli.headers['clientId'] = this.apickli.scenarioVariables.clientId;
        this.apickli.headers['clientVersion'] = 999; // ie, latest.
        this.apickli.headers['Content-Type'] = 'application/json';
        this.apickli.requestBody = JSON.stringify(this.apickli.scenarioVariables.currentRequest);
        this.apickli.post(pathSuffix, function (err, response) {
			//console.log(response.body);			
		var test  = JSON.parse(response.body);
		console.log(test);
		//var Reference = test.gatewayResponse.reference;
		//console.log("Trans Reference : "+Reference);
	//	this.apickli.storeValueOfResponseBodyPathInScenarioScope('$.gatewayResponse.reference', 'reference');
		//console.log("Trans Reference : "+ this.apickli.scenarioVariables.reference);		
            callback(err);
        });
    });

};
