var CryptoJS = require("crypto-js");

var hmacTools = function () {
		var self = this;

		self.hmac = function(apiSecret, verb, url, body, merchantId, nonce, timestamp) {
				var data = verb + url + body + merchantId + nonce + timestamp;
				var hashSign = CryptoJS.HmacSHA512(data, apiSecret);
				var hmac = CryptoJS.enc.Base64.stringify(hashSign);
				// console.log('hmacTools-DATA: ' + data);
				// console.log('HMAC: ' + hmac);
				// console.log('merchantId: ' + merchantId);
				return hmac;
		}

		self.nonce = function(length) {
				var text = "";
				var possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
				for(var i = 0; i < length; i++) {
						text += possible.charAt(Math.floor(Math.random() * possible.length));
				}
				return text;
		}
        
        self.encrypt = function(apiSecret, iv, salt, message) {
            var derivedPassword = CryptoJS.PBKDF2(apiSecret, salt, { keySize: 256/32, iterations: 1500, hasher: CryptoJS.algo.SHA1 });
            
            var encrypted = CryptoJS.AES.encrypt(
              message,
              derivedPassword,
              { iv: iv }
            );
            
            return encrypted.toString();
        }
        
        self.secureNonce = function(length){
            var iv = CryptoJS.lib.WordArray.random(length)
            var salt = CryptoJS.enc.Base64.stringify(CryptoJS.enc.Utf8.parse(CryptoJS.enc.Hex.stringify(iv)));
            
            return {
                iv: iv,
                salt: salt
            }
            
        }
}

module.exports = hmacTools;

